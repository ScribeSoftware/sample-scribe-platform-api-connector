//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using Core.Common;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Actions;
    using Core.ConnectorApi.ConnectionUI;
    using Core.ConnectorApi.Cryptography;
    using Core.ConnectorApi.Exceptions;
    using Core.ConnectorApi.Logger;
    using Core.ConnectorApi.Query;
    using Domain;
    using Http;
    using Metadata;

    [ScribeConnector(
        connectorTypeId: ConnectorSettings.TypeId,
        name: ConnectorSettings.ConnectorName, 
        description: ConnectorSettings.Description,
        connectorType: typeof(ScribeApiConnector),
        settingsUITypeName: GenericSettings.SettingsUITypeName,
        settingsUIVersion: GenericSettings.SettingsUIVersion,
        connectionUITypeName: GenericSettings.ScribeonlineGenericconnectionui,
        connectionUIVersion: GenericSettings.ConnectionUIVersion,
        xapFileName: GenericSettings.XapFileName,
        supportedSolutionRoles: new[] {"Scribe.IS2.Source", "Scribe.IS2.Target"},
        supportsCloud: ConnectorSettings.SupportsCloud,
        connectorVersion: ConnectorSettings.Version)]
    public class ScribeApiConnector : IConnector
    {
        private const string InvalidScribeApiUrlError = "Invalid Base URL to Scribe Online API.";
        private const string InvalidCredentialsError = "Invalid Username or Password.";
        private const string NotExistingOrgIdError = "Invalid Default Organization Id: organization with provided Id not exists.";
        private const string InvalidDefaultOrgIdError = "Invalid Default Organization Id: it should be non-negative integer value.";
        private const string ForbiddenDefaultOrgIdError = "Invalid Default Organization Id: you don't have access to organization with provided Id.";
        
        private const string UnknownError = "Unknown error occured.";

        private readonly Func<Uri, string, string, SingleMediaTypeHttpClient> httpClientFactory;
        private readonly IMetadataProvider metadataProvider;
        private SingleMediaTypeHttpClient scribeApiClient;
        private ScribeApiQueryExecutor queryExecutor;
        private ScribeApiOperationExecutor operationExecutor;

        public bool IsConnected { get; private set; }

        public Guid ConnectorTypeId
        {
            get { return Guid.Parse(ConnectorSettings.TypeId); }
        }

        public ScribeApiConnector() : this(ScribeApiClient.Create)
        {
        }

        public ScribeApiConnector(
            Func<Uri, string, string, SingleMediaTypeHttpClient> httpClientFactory)
        {
            if (httpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(httpClientFactory));
            }

            this.httpClientFactory = httpClientFactory;
            this.metadataProvider = new LoggingMetadataProvider(
                new CachingMetadataProvider(
                new AttributeBasedMetadataProvider(
                    Assembly.GetExecutingAssembly(),
                    t => t.Namespace == typeof(Organization).Namespace)), ConnectorSettings.ConnectorName);
        }

        public string PreConnect(IDictionary<string, string> properties)
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(PreConnect)))
            {
                var form = new FormDefinition
                {
                    CompanyName = ConnectorSettings.CompanyName,
                    CryptoKey = ConnectorSettings.CryptoKey,
                    HelpUri = new Uri(ConnectorSettings.HelpUriString),
                    Entries = new List<EntryDefinition>
                    {
                        new EntryDefinition
                        {
                            Label = "Base URL to Scribe API",
                            PropertyName = "ScribeApiUrl",
                            InputType = InputType.Text,
                            IsRequired = true,
                            Order = 0
                        },
                        new EntryDefinition
                        {
                            InputType = InputType.Text,
                            Label = "Default Organization ID",
                            PropertyName = "DefaultOrgId",
                            IsRequired = false,
                            Order = 1
                        },
                        new EntryDefinition
                        {
                            Label = "Username",
                            PropertyName = "Username",
                            InputType = InputType.Text,
                            IsRequired = true,
                            Order = 2
                        },
                        new EntryDefinition
                        {
                            Label = "Password",
                            PropertyName = "Password",
                            InputType = InputType.Password,
                            IsRequired = true,
                            Order = 3
                        }
                    }
                };

                return form.Serialize();
            }
        }

        public void Connect(IDictionary<string, string> properties)
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(Connect)))
            {
                IsConnected = false;
                try
                {
                    var baseUri = new Uri(properties["ScribeApiUrl"] ?? "");
                    var defaultOrgId =
                        string.IsNullOrWhiteSpace(properties["DefaultOrgId"])
                            ? null
                            : (int?) Convert.ToUInt32(properties["DefaultOrgId"]);

                    var username = properties["Username"] ?? "";
                    var password =
                        Decryptor.Decrypt_AesManaged(properties["Password"], ConnectorSettings.CryptoKey);
                    this.scribeApiClient =
                        this.httpClientFactory(baseUri, username, password);

                    // Ensure that ScribeApiUrl, Username and Password are valid.
                    // We need to call GET /v1/orgs and not GET /v1/orgs/{orgId} since
                    // we want to separate invalid ScribeApiUrl and invalid DefaultOrgId cases.
                    // Limiting to 1 org and reading only headers are just for performance purpose.
                    var testResponse = this.scribeApiClient.HttpClient
                        .GetAsync(
                            new Uri("/v1/orgs/?limit=1", UriKind.Relative),
                            HttpCompletionOption.ResponseHeadersRead)
                        .Result;

                    if (!testResponse.IsSuccessStatusCode)
                    {
                        switch (testResponse.StatusCode)
                        {
                            case HttpStatusCode.NotFound:
                                throw new InvalidConnectionException(
                                    InvalidScribeApiUrlError);
                            case HttpStatusCode.Unauthorized:
                                throw new InvalidConnectionException(
                                    InvalidCredentialsError);
                            default:
                                throw new InvalidConnectionException(UnknownError);
                        }
                    }

                    if (defaultOrgId != null)
                    {
                        var defaultOrgResponse = this.scribeApiClient.HttpClient
                            .GetAsync(
                                new Uri($"/v1/orgs/{defaultOrgId}", UriKind.Relative),
                                HttpCompletionOption.ResponseHeadersRead)
                            .Result;

                        if (!defaultOrgResponse.IsSuccessStatusCode)
                        {
                            switch (defaultOrgResponse.StatusCode)
                            {
                                case HttpStatusCode.Unauthorized:
                                    throw new InvalidConnectionException(
                                        InvalidCredentialsError);
                                case HttpStatusCode.Forbidden:
                                    throw new InvalidConnectionException(
                                        ForbiddenDefaultOrgIdError);
                                case HttpStatusCode.NotFound:
                                    throw new InvalidConnectionException(
                                        NotExistingOrgIdError);
                                default:
                                    throw new InvalidConnectionException(UnknownError);
                            }
                        }
                    }

                    this.queryExecutor = new ScribeApiQueryExecutor(
                        this.scribeApiClient,
                        new ExpressionVisitor(
                            throwOnOrLogicalOperator: true),
                        new CandidateValidator(
                            this.metadataProvider),
                        defaultOrgId);

                    this.operationExecutor = new ScribeApiOperationExecutor(
                        this.scribeApiClient,
                        this.queryExecutor,
                        defaultOrgId,
                        retryCount: 10,
                        delay: TimeSpan.FromSeconds(10));

                    IsConnected = true;
                }
                catch (AggregateException ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    // First call of GetBaseException will return first non-AggregateException.
                    // Second call will return the innermost exception.
                    var rootCause =
                        ex.GetBaseException().GetBaseException() as WebException;
                    if (rootCause != null && rootCause.Status ==
                        WebExceptionStatus.NameResolutionFailure)
                    {
                        throw new InvalidConnectionException(InvalidScribeApiUrlError);
                    }
                    throw new InvalidConnectionException(UnknownError);
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);

                    if (ex is UriFormatException)
                    {
                        throw new InvalidConnectionException(InvalidScribeApiUrlError);
                    }
                    if (ex is FormatException || ex is OverflowException)
                    {
                        throw new InvalidConnectionException(InvalidDefaultOrgIdError);
                    }
                    if (ex is InvalidConnectionException)
                    {
                        throw;
                    }

                    throw new InvalidConnectionException(UnknownError);
                }
            }
        }

        public void Disconnect()
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(Disconnect)))
            {
                IsConnected = false;
                this.scribeApiClient?.HttpClient?.Dispose();
            }
        }

        public IMetadataProvider GetMetadataProvider()
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(GetMetadataProvider)))
            {
                return this.metadataProvider;
            }
        }

        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(ExecuteQuery)))
            {
                try
                {
                    return this.queryExecutor.Execute(query).Result;
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    var rootCause = ex.FindRootCause();
                    if (rootCause is FatalErrorException)
                    {
                        throw new FatalErrorException(rootCause.Message, ex);
                    }
                    throw new InvalidExecuteQueryException(rootCause.Message);
                }
            }
        }

        public OperationResult ExecuteOperation(OperationInput input)
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(ExecuteOperation)))
            {
                try
                {
                    return this.operationExecutor.Execute(input).Result;
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    var rootCause = ex.FindRootCause();
                    if (rootCause is FatalErrorException)
                    {
                        throw new FatalErrorException(rootCause.Message, ex);
                    }
                    throw new InvalidExecuteOperationException(rootCause.Message);
                }
            }
        }

        public MethodResult ExecuteMethod(MethodInput input)
        {
            using (new LogMethodExecution(ConnectorSettings.ConnectorName, nameof(ExecuteMethod)))
            {
                throw new InvalidExecuteMethodException("Replication Services are not supported");
            }
        }
    }
}
