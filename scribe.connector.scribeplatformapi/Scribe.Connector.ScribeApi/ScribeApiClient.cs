//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Domain;
    using Http;
    using Newtonsoft.Json;
    using static System.Uri;

    public static class ScribeApiClient
    {
        public static SingleMediaTypeHttpClient Create(
            Uri baseAddress,
            string username,
            string password)
        {
            if (baseAddress == null)
            {
                throw new ArgumentNullException(nameof(baseAddress));
            }
            if (username == null)
            {
                throw new ArgumentNullException(nameof(username));
            }
            if (password == null)
            {
                throw new ArgumentNullException(nameof(password));
            }

            var httpMessageHandler =
                new DelayedHttpMessageHandler(TimeSpan.FromSeconds(1))
                    {InnerHandler = new HttpClientHandler()};

            var httpClient = new HttpClient(httpMessageHandler) {BaseAddress = baseAddress};
            httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.AcceptCharset.Add(
                new StringWithQualityHeaderValue("utf-8"));
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(
                        Encoding.UTF8.GetBytes($"{username}:{password}")));
            return new SingleMediaTypeHttpClient(
                httpClient,
                new JsonMediaTypeFormatter()
                {
                    SerializerSettings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }
                },
                ScribeApiResponseErrorAnalyzer.ThrowOnFailureStatusCode);
        }

        public static async Task<Organization> GetOrganizationByIdAsync(
            this SingleMediaTypeHttpClient client,
            int organizationId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, Organization>(
                HttpMethod.Get,
                new Uri($"/v1/orgs/{organizationId}", UriKind.Relative),
                token: token);
        }

        public static async Task<IEnumerable<Organization>> GetOrganizationsAsync(
            this SingleMediaTypeHttpClient client,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<Organization>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs?offset={offset}&limit={limit}",
                            UriKind.Relative), 
                        token: token),
                    limit));
        }

        public static async Task<IEnumerable<Solution>> GetSolutionsAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            string solutionName = null,
            string solutionType = null,
            string solutionStatus = null,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            var queryStr = new StringBuilder("?");
            if (!string.IsNullOrEmpty(solutionName))
            {
                queryStr.Append($"solutionNameFilter={EscapeDataString(solutionName)}&");
            }
            if (!string.IsNullOrEmpty(solutionType))
            {
                queryStr.Append($"solutionType={EscapeDataString(solutionType)}&");
            }
            if (!string.IsNullOrEmpty(solutionStatus))
            {
                queryStr.Append($"solutionStatusFilter={EscapeDataString(solutionStatus)}");
            }

            var apiUrl = $"/v1/orgs/{orgId}/solutions{queryStr}".TrimEnd('&');
            
            return await Task.FromResult(
                Paginated(
                    offset =>
                    {
                        var baseUrl = apiUrl.EndsWith("?")
                            ? $"{apiUrl}offset={offset}&limit={limit}"
                            : $"{apiUrl}&offset={offset}&limit={limit}";
                        return client.SendMediaContentAsync<object, IEnumerable<Solution>>(
                            HttpMethod.Get,
                            new Uri(baseUrl, UriKind.Relative),
                            token: token);
                    },
                    limit));
        }

        public static async Task<Solution> GetSolutionAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            
            return await client.SendMediaContentAsync<object, Solution>(
                HttpMethod.Get,
                new Uri($"/v1/orgs/{orgId}/solutions/{solutionId}", UriKind.Relative),
                token: token);
        }

        public static async Task<IReadOnlyCollection<InvitedUser>> GetInvitedUsersAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, List<InvitedUser>>(
                HttpMethod.Get,
                new Uri($"/v1/orgs/{orgId}/invitedusers", UriKind.Relative),
                token: token);
        }

        public static async Task<InvitedUser> GetInvitedUserAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            string email,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }
            
            return await client.SendMediaContentAsync<object, InvitedUser>(
                HttpMethod.Get,
                new Uri($"/v1/orgs/{orgId}/invitedusers/{EscapeDataString(email)}", UriKind.Relative),
                token: token);
        }

        public static async Task<IEnumerable<History>> GetHistoriesAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<History>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs/{orgId}"
                            + $"/solutions/{solutionId}"
                            + $"/history?offset={offset}&limit={limit}",
                            UriKind.Relative),
                        token: token),
                    limit));
        }

        public static async Task<History> GetHistoryAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            long historyId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, History>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{orgId}/solutions/{solutionId}/history/{historyId}",
                    UriKind.Relative),
                token: token);
        }


        public static async Task<IEnumerable<Error>> GetErrorsAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            long historyId,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<Error>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs/{orgId}/" 
                            + $"solutions/{solutionId}/" 
                            + $"history/{historyId}/" 
                            + $"errors?offset={offset}&limit={limit}",
                            UriKind.Relative),
                        token: token),
                    limit));
        }

        public static async Task<Error> GetErrorAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            long historyId,
            long errorId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            
            return await client.SendMediaContentAsync<object, Error>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{orgId}/solutions/{solutionId}"
                    + $"/history/{historyId}/errors/{errorId}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<Organization> CreateChildOrganizationAsync(
            this SingleMediaTypeHttpClient client,
            Organization child,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            return await client.SendMediaContentAsync<Organization, Organization>(
                HttpMethod.Post,
                new Uri("/v1/orgs", UriKind.Relative),
                child,
                token: token);
        }

        public static async Task<InvitedUser> CreateInvitedUserAsync(
            this SingleMediaTypeHttpClient client,
            InvitedUser invitedUser,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (invitedUser == null)
            {
                throw new ArgumentNullException(nameof(invitedUser));
            }

            return await client.SendMediaContentAsync<InvitedUser, InvitedUser>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{invitedUser.OrganizationId}/invitedusers",
                    UriKind.Relative),
                invitedUser,
                token: token);
        }

        public static async Task<Connection> CreateConnectionAsync(
            this SingleMediaTypeHttpClient client,
            Connection connection,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            return await client.SendMediaContentAsync<Connection, Connection>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{connection.OrganizationId}/connections",
                    UriKind.Relative),
                connection,
                token: token);
        }

        public static async Task<FailedRecord> GetFailedRecordAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            long historyId,
            long failedRecordId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, FailedRecord>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{orgId}/solutions/{solutionId}"
                    + $"/history/{historyId}/failedrecords/{failedRecordId}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<object> DeleteSolutionAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            
            return await client.SendMediaContentAsync<object, object>(
                HttpMethod.Delete,
                new Uri(
                    $"/v1/orgs/{orgId}/solutions/{solutionId}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<Solution> UpdateSolutionAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            Solution solution,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (solution == null)
            {
                throw new ArgumentNullException(nameof(solution));
            }
            
            return await client.SendMediaContentAsync<Solution, Solution>(
                HttpMethod.Put,
                new Uri(
                    $"/v1/orgs/{orgId}/solutions/{solutionId}",
                    UriKind.Relative),
                solution,
                token: token);
        }

        public static async Task<IEnumerable<OrganizationUser>> GetOrganizationUsersAsync(
            this SingleMediaTypeHttpClient client,
            int organizationId,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<OrganizationUser>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs/{organizationId}/users?offset={offset}&limit={limit}",
                            UriKind.Relative),
                        token: token),
                    limit));
        }

        public static async Task<object> DeleteOrganizationUserAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            string email,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }
            
            return await client.SendMediaContentAsync<object, object>(
                HttpMethod.Delete,
                new Uri(
                    $"/v1/orgs/{orgId}/users?email={EscapeDataString(email)}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<CloneSolution> CloneSolutionAsync(
            this SingleMediaTypeHttpClient client,
            CloneSolution cloneSolutionCommand,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (cloneSolutionCommand == null)
            {
                throw new ArgumentNullException(nameof(cloneSolutionCommand));
            }
            
            return await client.SendMediaContentAsync<object, CloneSolution>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{cloneSolutionCommand.OrganizationId}"
                    + $"/solutions/{cloneSolutionCommand.SolutionId}/clone"
                    + $"?destOrgId={cloneSolutionCommand.DestinationOrganizationId}"
                    + $"&destAgentId={cloneSolutionCommand.DestinationAgentId}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<PrepareSolution> PrepareSolutionAsync(
            this SingleMediaTypeHttpClient client,
            PrepareSolution prepareSolutionCommand,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (prepareSolutionCommand == null)
            {
                throw new ArgumentNullException(nameof(prepareSolutionCommand));
            }
            
            return await client.SendMediaContentAsync<object, PrepareSolution>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{prepareSolutionCommand.OrganizationId}"
                    + $"/solutions/{prepareSolutionCommand.SolutionId}/prepare",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<PrepareSolution> GetSolutionPreparationResultAsync(
            this SingleMediaTypeHttpClient client,
            PrepareSolution command,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }
            
            return await client.SendMediaContentAsync<object, PrepareSolution>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{command.OrganizationId}"
                    + $"/solutions/{command.SolutionId}"
                    + $"/prepare/{command.Id}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<object> StartSolutionAsync(
            this SingleMediaTypeHttpClient client,
            StartSolution startSolutionCommand,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (startSolutionCommand == null)
            {
                throw new ArgumentNullException(nameof(startSolutionCommand));
            }
            
            return await client.SendMediaContentAsync<StartSolution, object>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{startSolutionCommand.OrganizationId}"
                    + $"/solutions/{startSolutionCommand.SolutionId}/start",
                    UriKind.Relative),
                token: token);
        }


        public static async Task<object> StopSolutionAsync(
            this SingleMediaTypeHttpClient client,
            StopSolution stopSolutionCommand,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (stopSolutionCommand == null)
            {
                throw new ArgumentNullException(nameof(stopSolutionCommand));
            }

            return await client.SendMediaContentAsync<StopSolution, object>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{stopSolutionCommand.OrganizationId}"
                    + $"/solutions/{stopSolutionCommand.SolutionId}/stop",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<object> MarkErrorsAsync(
            this SingleMediaTypeHttpClient client,
            MarkAllErrors command,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return await client.SendMediaContentAsync<object, MarkAllErrors>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{command.OrganizationId}"
                    + $"/solutions/{command.SolutionId}"
                    + $"/history/{command.HistoryId}"
                    + $"/mark?mark={command.Mark}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<object> ReprocessErrorsAsync(
            this SingleMediaTypeHttpClient client,
            ReprocessAllErrors command,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return await client.SendMediaContentAsync<object, ReprocessAllErrors>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{command.OrganizationId}"
                    + $"/solutions/{command.SolutionId}"
                    + $"/history/{command.HistoryId}/reprocess",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<IEnumerable<Map>> GetMapsAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<Map>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs/{orgId}/solutions/{solutionId}/maps?offset={offset}&limit={limit}",
                            UriKind.Relative),
                        token: token),
                    limit));
        }
        
        public static async Task<Map> GetMapAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid solutionId,
            int mapId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            
            return await client.SendMediaContentAsync<object, Map>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{orgId}/solutions/{solutionId}/maps/{mapId}",
                    UriKind.Relative),
                token: token);
        }
        
        public static async Task<RunMap> RunMapAsync(
            this SingleMediaTypeHttpClient client,
            RunMap command,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }
            
            return await client.SendMediaContentAsync<object, RunMap>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{command.OrganizationId}"
                    + $"/solutions/{command.SolutionId}"
                    + $"/maps/{command.MapId}/run",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<Connection> GetConnectionAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid connectionId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, Connection>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{orgId}/connections/{connectionId}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<IEnumerable<Connection>> GetConnectionsAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            string name = null,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            var baseUri = name == null
                ? $"/v1/orgs/{orgId}/connections?expand=false&"
                : $"/v1/orgs/{orgId}/connections?expand=false&name={EscapeDataString(name)}&";

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<Connection>>(
                        HttpMethod.Get,
                        new Uri(
                            $"{baseUri}offset={offset}&limit={limit}",
                            UriKind.Relative), 
                        token: token),
                    limit));
        }

        public static async Task<TestConnection> TestConnectionAsync(
            this SingleMediaTypeHttpClient client,
            TestConnection command,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return await client.SendMediaContentAsync<object, TestConnection>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{command.OrganizationId}" +
                    $"/connections/{command.ConnectionId}" +
                    $"/test?agentId={command.AgentId}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<TestConnection> GetTestConnectionResultAsync(
            this SingleMediaTypeHttpClient client,
            TestConnection command,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return await client.SendMediaContentAsync<object, TestConnection>(
                HttpMethod.Get,
                new Uri(
                    $"/v1/orgs/{command.OrganizationId}/connections/test/{command.Id}",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<IEnumerable<Connector>> GetConnectorsAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<Connector>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs/{orgId}/connectors?offset={offset}&limit={limit}",
                            UriKind.Relative),
                        token: token),
                    limit));
        }

        public static async Task<Connector> GetConnectorAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid connectorId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, Connector>(
                HttpMethod.Get,
                new Uri($"/v1/orgs/{orgId}/connectors/{connectorId}", UriKind.Relative),
                token: token);
        }


        public static async Task<object> InstallConnectorAsync(
            this SingleMediaTypeHttpClient client,
            InstallConnector installConnector,
            CancellationToken token = default(CancellationToken))
        {
            if (installConnector == null)
            {
                throw new ArgumentNullException(nameof(installConnector));
            }

            return await client.SendMediaContentAsync<object, object>(
                HttpMethod.Post,
                new Uri(
                    $"/v1/orgs/{installConnector.OrganizationId}"
                    + $"/connectors/{installConnector.Id}/install",
                    UriKind.Relative),
                token: token);
        }

        public static async Task<IEnumerable<Agent>> GetAgentsAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            int limit = 100,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await Task.FromResult(
                Paginated(
                    offset => client.SendMediaContentAsync<object, IEnumerable<Agent>>(
                        HttpMethod.Get,
                        new Uri(
                            $"/v1/orgs/{orgId}/agents?offset={offset}&limit={limit}",
                            UriKind.Relative),
                        token: token),
                    limit));
        }

        public static async Task<Agent> GetAgentAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            Guid agentId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, Agent>(
                HttpMethod.Get,
                new Uri($"/v1/orgs/{orgId}/agents/{agentId}", UriKind.Relative),
                token: token);
        }

        public static async Task<object> ProvisionCloudAgentAsync(
            this SingleMediaTypeHttpClient client,
            int orgId,
            CancellationToken token = default(CancellationToken))
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return await client.SendMediaContentAsync<object, object>(
                HttpMethod.Post,
                new Uri($"/v1/orgs/{orgId}/agents/provision_cloud_agent", UriKind.Relative),
                token: token);
        }

        private static IEnumerable<TOutput> Paginated<TOutput>(
            Func<int, Task<IEnumerable<TOutput>>> retrievePage,
            int limit,
            int offset = 0)
        {
            bool hasMore;
            do
            {
                var page = retrievePage(offset).GetAwaiter().GetResult()?.ToList()
                    ?? new List<TOutput>(0);
                
                foreach (var entity in page)
                {
                    yield return entity;
                }
                hasMore = page.Count == limit;
                offset += limit;
            } while (hasMore);
        }
    }
}
