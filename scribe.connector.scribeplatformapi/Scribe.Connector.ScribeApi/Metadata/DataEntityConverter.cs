//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Query;

    public static class DataEntityConverter
    {
        public static DataEntity ToDataEntity<T>(this T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var type = value.GetType();
            var queryDataEntity = new QueryDataEntity
            {
                ObjectDefinitionFullName = type.Name,
                Name = type.Name
            };

            type.GetProperties()
                .Where(p => p.GetCustomAttributes<PropertyDefinitionAttribute>().Any())
                .ToList()
                .ForEach(p => queryDataEntity.Properties.Add(p.Name, p.GetValue(value)));
            type.GetFields()
                .Where(f => f.GetCustomAttributes<PropertyDefinitionAttribute>().Any())
                .ToList()
                .ForEach(f => queryDataEntity.Properties.Add(f.Name, f.GetValue(value)));

            return queryDataEntity.ToDataEntity();
        }

        public static T ToDomainObject<T>(this DataEntity entity) where T : new()
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var properties = typeof(T).GetProperties()
                .Where(p => p.GetCustomAttributes<PropertyDefinitionAttribute>().Any())
                .ToDictionary(
                    keySelector: p => p.Name,
                    elementSelector: p => p);
            var fields = typeof(T).GetFields()
                .Where(f => f.GetCustomAttributes<PropertyDefinitionAttribute>().Any())
                .ToDictionary(
                    keySelector: f => f.Name,
                    elementSelector: f => f);

            var result = new T();
            foreach (var source in entity.Properties)
            {
                if (source.Value == null)
                {
                    continue;
                }
                if (properties.ContainsKey(source.Key))
                {
                    var property = properties[source.Key];
                    var value = property.PropertyType == source.Value.GetType()
                        ? source.Value
                        : Convert
                            .ToString(source.Value, CultureInfo.InvariantCulture)
                            .Parse(property.PropertyType);
                    properties[source.Key].SetValue(result, value);
                }
                if (fields.ContainsKey(source.Key))
                {
                    var field = fields[source.Key];
                    var value = field.FieldType == source.Value.GetType()
                        ? source.Value
                        : Convert
                            .ToString(source.Value, CultureInfo.InvariantCulture)
                            .Parse(field.FieldType);
                    fields[source.Key].SetValue(result, value);
                }
            }
            return result;
        }
    }
}
