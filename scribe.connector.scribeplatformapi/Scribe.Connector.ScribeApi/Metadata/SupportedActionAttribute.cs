//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.ComponentModel;
    using Core.ConnectorApi.Metadata;

    [AttributeUsage(
        validOn: AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface,
        AllowMultiple = true,
        Inherited = false)]
    public class SupportedActionAttribute : Attribute, IActionDefinition
    {
        public SupportedActionAttribute(KnownActions knownActionType)
        {
            if (!Enum.IsDefined(typeof(KnownActions), knownActionType))
            {
                throw new InvalidEnumArgumentException(
                    nameof(knownActionType),
                    (int) knownActionType,
                    typeof(KnownActions));
            }

            KnownActionType = knownActionType;
            FullName = knownActionType.ToString();
            Name = FullName;
            Description = FullName;
        }

        public string FullName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public KnownActions KnownActionType { get; set; }
        public bool SupportsLookupConditions { get; set; }
        public bool SupportsInput { get; set; }
        public bool SupportsBulk { get; set; }
        public bool SupportsMultipleRecordOperations { get; set; }
        public bool SupportsSequences { get; set; }
        public bool SupportsConstraints { get; set; }
        public bool SupportsRelations { get; set; }
    }
}
