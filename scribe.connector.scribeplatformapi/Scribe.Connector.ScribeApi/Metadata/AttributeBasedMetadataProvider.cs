//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Metadata;

    public class AttributeBasedMetadataProvider : IMetadataProvider
    {
        private readonly IEqualityComparer<IActionDefinition> actionDefinitionComparer;

        public Assembly EntityAssembly { get; }
        public Func<Type, bool> TypeFilter { get; }

        public AttributeBasedMetadataProvider(
            Assembly entityAssembly,
            Func<Type, bool> typeFilter)
        {
            if (entityAssembly == null)
            {
                throw new ArgumentNullException(nameof(entityAssembly));
            }
            if (typeFilter == null)
            {
                throw new ArgumentNullException(nameof(typeFilter));
            }

            EntityAssembly = entityAssembly;
            TypeFilter = typeFilter;
            this.actionDefinitionComparer = new FullNameAndTypeActionDefinitionEqualityComparer();
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            return EntityAssembly.GetTypes()
                .Where(
                    t => TypeFilter(t)
                    && t.GetCustomAttributes<ObjectDefinitionAttribute>()
                        .Any(o => !o.Hidden))
                .SelectMany(t => t.GetCustomAttributes<SupportedActionAttribute>())
                .Distinct(this.actionDefinitionComparer);
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(
            bool shouldGetProperties = false,
            bool shouldGetRelations = false)
        {
            var candidates = EntityAssembly.GetTypes()
                .Where(
                    t => TypeFilter(t)
                    && t.GetCustomAttributes<ObjectDefinitionAttribute>().Any())
                .ToDictionary(
                    keySelector: t => t.GetTypeInfo(),
                    elementSelector: t => t.GetCustomAttribute<ObjectDefinitionAttribute>());

            foreach (var candidate in candidates)
            {
                candidate.Value.FullName = string.IsNullOrWhiteSpace(candidate.Value.FullName)
                    ? candidate.Key.Name
                    : candidate.Value.FullName.Trim();
                candidate.Value.SupportedActionFullNames = candidate.Key
                    .GetCustomAttributes<SupportedActionAttribute>()
                    .Select(a => a.FullName)
                    .ToList();
                candidate.Value.PropertyDefinitions = new List<IPropertyDefinition>();

                if (!shouldGetProperties)
                {
                    continue;
                }

                var propertyCandidates = candidate.Key.GetMembers()
                    .Where(m => m.GetCustomAttributes<PropertyDefinitionAttribute>().Any())
                    .ToDictionary(
                        keySelector: m => m,
                        elementSelector: m => m.GetCustomAttribute<PropertyDefinitionAttribute>());

                foreach (var propertyCandidate in propertyCandidates)
                {
                    propertyCandidate.Value.FullName =
                        string.IsNullOrWhiteSpace(propertyCandidate.Value.FullName)
                            ? propertyCandidate.Key.Name
                            : propertyCandidate.Value.FullName.Trim();

                    FillCandidateByMemberInfo(propertyCandidate.Key, propertyCandidate.Value);
                    candidate.Value.PropertyDefinitions.Add(propertyCandidate.Value);
                }
            }

            return candidates.Values;
        }

        private static void FillCandidateByMemberInfo(MemberInfo info, IPropertyDefinition candidate)
        {
            var isField = info as FieldInfo;
            if (isField != null)
            {
                var fieldInfo = (FieldInfo) info;
                candidate.PropertyType = fieldInfo.FieldType.GetTypeName();
                candidate.Nullable = !candidate.IsPrimaryKey
                    && (fieldInfo.FieldType.IsClass
                        || fieldInfo.FieldType.IsInterface
                        || fieldInfo.FieldType.IsNullable()
                        || !fieldInfo.FieldType.IsValueType);
                return;
            }

            var isProperty = info as PropertyInfo;
            if (isProperty != null)
            {
                var propertyInfo = (PropertyInfo) info;
                candidate.PropertyType = propertyInfo.PropertyType.GetTypeName();
                candidate.Nullable = !candidate.IsPrimaryKey
                    && (propertyInfo.PropertyType.IsClass
                        || propertyInfo.PropertyType.IsInterface
                        || propertyInfo.PropertyType.IsNullable()
                        || !propertyInfo.PropertyType.IsValueType);
            }
        }

        public IObjectDefinition RetrieveObjectDefinition(
            string objectName,
            bool shouldGetProperties = false,
            bool shouldGetRelations = false)
        {
            return RetrieveObjectDefinitions(shouldGetProperties, shouldGetRelations)
                .Single(o => o.FullName == objectName);
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(
            bool shouldGetParameters = false)
        {
            throw new InvalidOperationException("Replication Services are not supported");
        }

        public IMethodDefinition RetrieveMethodDefinition(
            string objectName,
            bool shouldGetParameters = false)
        {
            throw new InvalidOperationException("Replication Services are not supported");
        }

        public void ResetMetadata()
        {
        }

        public void Dispose()
        {
        }
    }
}
