//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Collections.Generic;
    using Core.Common;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Logger;
    using Core.ConnectorApi.Metadata;

    public class LoggingMetadataProvider : IMetadataProvider
    {
        public LoggingMetadataProvider(
            IMetadataProvider decoratedProvider,
            string connectorName)
        {
            if (decoratedProvider == null)
            {
                throw new ArgumentNullException(nameof(decoratedProvider));
            }
            if (connectorName == null)
            {
                throw new ArgumentNullException(nameof(connectorName));
            }

            DecoratedProvider = decoratedProvider;
            ConnectorName = connectorName;
        }

        public IMetadataProvider DecoratedProvider { get; }
        public string ConnectorName { get; }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            using (new LogMethodExecution(ConnectorName, nameof(RetrieveActionDefinitions)))
            {
                try
                {
                    return DecoratedProvider.RetrieveActionDefinitions();
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    throw;
                }
            }
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(
            bool shouldGetProperties = false,
            bool shouldGetRelations = false)
        {
            using (new LogMethodExecution(ConnectorName, nameof(RetrieveObjectDefinitions)))
            {
                try
                {
                    return DecoratedProvider.RetrieveObjectDefinitions(
                        shouldGetProperties,
                        shouldGetRelations);
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    throw;
                }
            }
        }

        public IObjectDefinition RetrieveObjectDefinition(
            string objectName,
            bool shouldGetProperties = false,
            bool shouldGetRelations = false)
        {
            using (new LogMethodExecution(ConnectorName, nameof(RetrieveObjectDefinition)))
            {
                try
                {
                    return DecoratedProvider.RetrieveObjectDefinition(
                        objectName,
                        shouldGetProperties,
                        shouldGetRelations);
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    throw;
                }
            }
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(
            bool shouldGetParameters = false)
        {
            using (new LogMethodExecution(ConnectorName, nameof(RetrieveMethodDefinitions)))
            {
                try
                {
                    return DecoratedProvider.RetrieveMethodDefinitions(shouldGetParameters);
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    throw;
                }
            }
        }

        public IMethodDefinition RetrieveMethodDefinition(
            string objectName,
            bool shouldGetParameters = false)
        {
            using (new LogMethodExecution(ConnectorName, nameof(RetrieveMethodDefinition)))
            {
                try
                {
                    return DecoratedProvider.RetrieveMethodDefinition(
                        objectName,
                        shouldGetParameters);
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    throw;
                }
            }
        }

        public void ResetMetadata()
        {
            using (new LogMethodExecution(ConnectorName, nameof(ResetMetadata)))
            {
                try
                {
                    DecoratedProvider.ResetMetadata();
                }
                catch (Exception ex)
                {
                    ScribeLogger.Instance.WriteException(this, ex);
                    throw;
                }
            }
        }

        public void Dispose()
        {
        }
    }
}
