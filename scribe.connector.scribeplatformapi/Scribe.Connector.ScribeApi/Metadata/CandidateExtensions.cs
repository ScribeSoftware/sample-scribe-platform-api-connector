//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Collections.Generic;
    using Core.ConnectorApi.Exceptions;
    using static ReflectionExtensions;

    public static class CandidateExtensions
    {
        private const string RequiredPropertyIsMissingInQueryErrorFormat =
            "Filter or lookup condition don't contain required {0} property for {1} entity.";
        
        public static string GetQueryEntityPropertyName<TEntity>(string propertyName)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }
            
            var objectDefinition = GetObjectDefinitionFullName<TEntity>();
            var propertyDefinition = GetPropertyDefinitionFullName<TEntity>(propertyName);
            return $"{objectDefinition}.{propertyDefinition}";
        }
        
        public static TParsedValue GetValueOrDefault<TEntity, TParsedValue>(
            this IDictionary<string, string> dictionary,
            string propertyName,
            TParsedValue defaultValue = default(TParsedValue))
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }

            var key = GetQueryEntityPropertyName<TEntity>(propertyName);
            return dictionary.ContainsKey(key)
                ? dictionary[key].Parse<TParsedValue>()
                : defaultValue;
        }
        
        public static TParsedValue GetRequiredValueOrThrow<TEntity, TParsedValue>(
            this IDictionary<string, string> dictionary,
            string propertyName,
            TParsedValue defaultValue = default(TParsedValue))
        {
            var value = dictionary.GetValueOrDefault<TEntity, TParsedValue>(
                propertyName, 
                defaultValue);

            if (Equals(value, default(TParsedValue)))
            {
                return !Equals(defaultValue, default(TParsedValue)) 
                    ? defaultValue
                    : throw new FatalErrorException(
                        string.Format(
                            RequiredPropertyIsMissingInQueryErrorFormat,
                            GetPropertyDefinitionFullName<TEntity>(propertyName),
                            GetObjectDefinitionFullName<TEntity>())); 
            }

            return value;
        }
    }
}
