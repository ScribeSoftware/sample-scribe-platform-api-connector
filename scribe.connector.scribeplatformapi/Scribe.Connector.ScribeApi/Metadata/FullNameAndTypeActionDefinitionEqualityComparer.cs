//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System.Collections.Generic;
    using Core.ConnectorApi.Metadata;

    public sealed class FullNameAndTypeActionDefinitionEqualityComparer : IEqualityComparer<IActionDefinition>
    {
        public bool Equals(IActionDefinition x, IActionDefinition y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            if (ReferenceEquals(x, null))
            {
                return false;
            }
            if (ReferenceEquals(y, null))
            {
                return false;
            }
            if (x.GetType() != y.GetType())
            {
                return false;
            }

            return string.Equals(x.FullName, y.FullName)
                && string.Equals(x.Description, y.Description);
        }

        public int GetHashCode(IActionDefinition obj)
        {
            unchecked
            {
                var hashCode = obj.FullName?.GetHashCode() ?? 0;
                hashCode = (hashCode * 397) ^ (obj.Description?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}
