//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Linq;
    using System.Reflection;

    public static class ReflectionExtensions
    {
        public static bool IsNullable(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return type.IsGenericType
                && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        public static string GetTypeName(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return IsNullable(type)
                ? type.GenericTypeArguments.First().FullName
                : type.FullName;
        }

        public static string GetObjectDefinitionFullName<T>()
        {
            return typeof(T).GetObjectDefinitionFullName();
        }

        public static string GetObjectDefinitionFullName(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var definitions = type.GetCustomAttributes<ObjectDefinitionAttribute>().ToList();

            if (!definitions.Any())
            {
                throw new ArgumentException(
                    $"Can't find object definition for {type.FullName} type",
                    nameof(type));
            }
            
            return string.IsNullOrWhiteSpace(definitions.Single().FullName)
                ? type.Name
                : definitions.Single().FullName.Trim();
        }

        public static string GetPropertyDefinitionFullName<T>(string propertyName)
        {
            return typeof(T).GetPropertyDefinitionFullName(propertyName);
        }
        
        public static string GetPropertyDefinitionFullName(this Type type, string propertyName)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            var definitions = type.GetMembers()
                .Where(m => m.Name == propertyName)
                .SelectMany(m => m.GetCustomAttributes<PropertyDefinitionAttribute>())
                .ToList();

            if (!definitions.Any())
            {
                throw new ArgumentException(
                    $"Can't find property definition for {propertyName} property in {type.FullName} type",
                    nameof(propertyName));
            }
            
            return string.IsNullOrWhiteSpace(definitions.Single().FullName)
                ? propertyName
                : definitions.Single().FullName.Trim();
        }
    }
}
