//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Metadata;

    public class CandidateValidator
    {
        private const string UnknownPropertyDefinitionErrorFormat = "Can't find property definition for {0} in metadata.";
        private const string UnknownPropertyTypeErrorFormat = "Can't find type {0} for property {1}.";
        private const string ConvertPropertyValueErrorFormat = "Can't convert {0} to type {1} for property {2}.";
        
        private readonly IMetadataProvider metadataProvider;

        public CandidateValidator(IMetadataProvider metadataProvider)
        {
            if (metadataProvider == null)
            {
                throw new ArgumentNullException(nameof(metadataProvider));
            }
            
            this.metadataProvider = metadataProvider;
        }

        public IDictionary<string, string> Validate(IDictionary<string, string> candidate)
        {
            if (candidate == null)
            {
                throw new ArgumentNullException(nameof(candidate));
            }
            if (!candidate.Any())
            {
                return new Dictionary<string, string>(0);
            }
            
            var objectDefinitions = this.metadataProvider
                .RetrieveObjectDefinitions(
                    shouldGetProperties: true,
                    shouldGetRelations: true);
            
            var propertyDefinitions = new Dictionary<string, IPropertyDefinition>();
            foreach (var objectDefinition in objectDefinitions)
            {
                foreach (var propertyDefinition in objectDefinition.PropertyDefinitions)
                {
                    propertyDefinitions.Add(
                        $"{objectDefinition.FullName}.{propertyDefinition.FullName}",
                        propertyDefinition);
                }
            }

            var errors = new Dictionary<string, string>();
            foreach (var propertyValue in candidate)
            {
                if (!propertyDefinitions.ContainsKey(propertyValue.Key))
                {
                    errors[propertyValue.Key] = string.Format(
                        UnknownPropertyDefinitionErrorFormat,
                        propertyValue.Key);
                    continue;
                }
                
                var propertyDefinition = propertyDefinitions[propertyValue.Key];
                if (propertyDefinition.Nullable && string.IsNullOrWhiteSpace(propertyValue.Value))
                {
                    continue;
                }

                var type = Type.GetType(propertyDefinition.PropertyType);
                if (type == null)
                {
                    errors[propertyValue.Key] = string.Format(
                        UnknownPropertyTypeErrorFormat, 
                        propertyDefinition.PropertyType,
                        propertyValue.Key);
                    continue;
                }

                if (!propertyDefinition.Nullable 
                    && type.IsValueType 
                    && string.IsNullOrWhiteSpace(propertyValue.Value))
                {
                    errors[propertyValue.Key] = string.Format(
                        ConvertPropertyValueErrorFormat, 
                        propertyValue.Value ?? "'null'",
                        type.FullName, 
                        propertyValue.Key);
                    continue;
                }

                if (!propertyValue.Value.CanParseTo(type))
                {
                    errors[propertyValue.Key] = string.Format(
                        ConvertPropertyValueErrorFormat, 
                        propertyValue.Value,
                        type.FullName, 
                        propertyValue.Key);
                }
            }
            return errors;
        }
    }
}
