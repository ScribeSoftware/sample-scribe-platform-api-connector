//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Metadata;

    public class CachingMetadataProvider : IMetadataProvider
    {
        private IReadOnlyCollection<IActionDefinition> actionCache;
        private IDictionary<string, IObjectDefinition> objectCache;
        private IDictionary<string, IMethodDefinition> methodCache;

        public CachingMetadataProvider(IMetadataProvider decoratedProvider)
        {
            if (decoratedProvider == null)
            {
                throw new ArgumentNullException(nameof(decoratedProvider));
            }

            this.DecoratedProvider = decoratedProvider;
            ResetMetadata();
        }
        
        public IMetadataProvider DecoratedProvider { get; }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            if (!this.actionCache.Any())
            {
                this.actionCache = this.DecoratedProvider.RetrieveActionDefinitions().ToList();
            }
            return this.actionCache;
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(
            bool shouldGetProperties = false,
            bool shouldGetRelations = false)
        {
            if (!this.objectCache.Any())
            {
                WarmUpObjectCache();
            }

            return shouldGetProperties && shouldGetRelations
                ? this.objectCache.Values
                : this.objectCache.Values.Select(
                    o => Copy(o, shouldGetProperties, shouldGetRelations));
        }

        private static IObjectDefinition Copy(
            IObjectDefinition source,
            bool shouldGetProperties,
            bool shouldGetRelations)
        {
            return new ObjectDefinition
            {
                Description = source.Description,
                FullName = source.FullName,
                Hidden = source.Hidden,
                Name = source.Name,
                PropertyDefinitions = shouldGetProperties
                    ? source.PropertyDefinitions
                    : new List<IPropertyDefinition>(0),
                RelationshipDefinitions = shouldGetRelations
                    ? source.RelationshipDefinitions
                    : new List<IRelationshipDefinition>(0),
                SupportedActionFullNames = source.SupportedActionFullNames
            };
        }

        private void WarmUpObjectCache()
        {
            this.objectCache = this.DecoratedProvider
                .RetrieveObjectDefinitions(
                    shouldGetProperties: true,
                    shouldGetRelations: true)
                .ToDictionary(
                    keySelector: o => o.FullName);
        }

        public IObjectDefinition RetrieveObjectDefinition(
            string objectName,
            bool shouldGetProperties = false,
            bool shouldGetRelations = false)
        {
            if (!this.objectCache.Any())
            {
                WarmUpObjectCache();
            }

            if (!this.objectCache.ContainsKey(objectName))
            {
                throw new ArgumentException(
                    $"ObjectDefinition with {objectName} name was not found",
                    nameof(objectName));
            }

            return shouldGetProperties && shouldGetRelations
                ? this.objectCache[objectName]
                : Copy(
                    this.objectCache[objectName],
                    shouldGetProperties,
                    shouldGetRelations);
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(
            bool shouldGetParameters = false)
        {
            if (!this.methodCache.Any())
            {
                WarmUpMethodCache();
            }

            return shouldGetParameters
                ? this.methodCache.Values
                : this.methodCache.Values.Select(m => Copy(m, false));
        }

        private static IMethodDefinition Copy(
            IMethodDefinition source,
            bool shouldGetParameters)
        {
            return new MethodDefinition
            {
                Description = source.Description,
                FullName = source.FullName,
                InputObjectDefinition = shouldGetParameters
                    ? source.InputObjectDefinition
                    : null,
                OutputObjectDefinition = shouldGetParameters
                    ? source.OutputObjectDefinition
                    : null,
                Name = source.Name
            };
        }

        private void WarmUpMethodCache()
        {
            this.methodCache = this.DecoratedProvider
                .RetrieveMethodDefinitions(shouldGetParameters: true)
                .ToDictionary(keySelector: m => m.FullName);
        }

        public IMethodDefinition RetrieveMethodDefinition(
            string objectName,
            bool shouldGetParameters = false)
        {
            if (!this.methodCache.Any())
            {
                WarmUpMethodCache();
            }

            if (!this.methodCache.ContainsKey(objectName))
            {
                throw new ArgumentException(
                    $"ObjectDefinition with {objectName} name was not found",
                    nameof(objectName));
            }

            return shouldGetParameters
                ? this.methodCache[objectName]
                : Copy(this.methodCache[objectName], false);
        }

        public void ResetMetadata()
        {
            this.actionCache = new List<IActionDefinition>(0);
            this.objectCache = new Dictionary<string, IObjectDefinition>(0);
            this.methodCache = new Dictionary<string, IMethodDefinition>(0);
        }

        public void Dispose()
        {
        }
    }
}
