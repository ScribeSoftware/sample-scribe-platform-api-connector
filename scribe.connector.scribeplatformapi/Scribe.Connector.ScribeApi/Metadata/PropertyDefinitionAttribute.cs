//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

namespace Scribe.Connector.ScribeApi.Metadata
{
    using System;
    using Core.ConnectorApi.Metadata;

    [AttributeUsage(
        validOn: AttributeTargets.Field | AttributeTargets.Property,
        AllowMultiple = false,
        Inherited = true)]
    public class PropertyDefinitionAttribute : Attribute, IPropertyDefinition
    {
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PropertyType { get; set; }
        public int MinOccurs { get; set; }
        public int MaxOccurs { get; set; }
        public int Size { get; set; }
        public int NumericScale { get; set; }
        public int NumericPrecision { get; set; }
        public string PresentationType { get; set; }
        public bool Nullable { get; set; }
        public bool IsPrimaryKey { get; set; }
        public bool UsedInQuerySelect { get; set; } = true;
        public bool UsedInQueryConstraint { get; set; }
        public bool UsedInActionInput { get; set; } = true;
        public bool UsedInActionOutput { get; set; } = true;
        public bool UsedInLookupCondition { get; set; }
        public bool UsedInQuerySequence { get; set; } = true;
        public bool RequiredInActionInput { get; set; }
    }
}
