//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
namespace Scribe.Connector.ScribeApi
{
    public static class ConnectorSettings
    {
        // TODO: Replace all of these values with ones that are specific to your connector.
        public const string TypeId = "REPLACE ME";

        public const string ConnectorName = "Scribe Platform API Sample";
        //Should be updated based on your implementation
        public const string CryptoKey = "E8DFC172-F6F2-482D-BBD2-7AD175FE40EA";
        public const string Description = "Scribe Connector for Scribe Platform API";
        public const bool SupportsCloud = true;
        public const string CompanyName = "Scribe Software";
        public const string HelpUriString = "https://help.scribesoft.com/scribe/en/index.htm#sol/conn/scribe_api.htm";
        public const string Version = "1.0.0.1";
    }
}