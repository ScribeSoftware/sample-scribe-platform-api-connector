//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Threading.Tasks;
    using Core.Common;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Actions;
    using Core.ConnectorApi.Exceptions;
    using Core.ConnectorApi.Metadata;
    using Core.ConnectorApi.Query;
    using Domain;
    using Http;
    using Metadata;
    using static Metadata.ReflectionExtensions;

    public class ScribeApiOperationExecutor
    {
        private const string BulkOperationsIsNotSupportedError =
            "Bulk operations are not supported.";

        private const string OperationIsNotSupportedErrorFormat =
            "{0} operation is not supported for entity {1}.";

        private const string RequiredPropertyIsMissingInInputErrorFormat =
            "Operation input doesn't contain required {0} property for {1} entity.";
        
        private const string MoreThanOneEntityFoundErrorFormat =
            "Found more than one entity while executing operation {0} {1}.";

        private readonly SingleMediaTypeHttpClient httpClient;
        private readonly ScribeApiQueryExecutor queryExecutor;
        private readonly int? defaultOrganizationId;
        private readonly int retryCount;
        private readonly TimeSpan delay;

        public ScribeApiOperationExecutor(
            SingleMediaTypeHttpClient httpClient,
            ScribeApiQueryExecutor queryExecutor,
            int? defaultOrganizationId,
            int retryCount,
            TimeSpan delay)
        {
            if (httpClient == null)
            {
                throw new ArgumentNullException(nameof(httpClient));
            }
            if (queryExecutor == null)
            {
                throw new ArgumentNullException(nameof(queryExecutor));
            }

            this.httpClient = httpClient;
            this.queryExecutor = queryExecutor;
            this.defaultOrganizationId = defaultOrganizationId;
            this.retryCount = retryCount;
            this.delay = delay;
        }

        public async Task<OperationResult> Execute(OperationInput input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }
            if (input.Input.Length == 0)
            {
                return new OperationResult(size: 0);
            }
            if (input.Input.Length > 1)
            {
                throw new FatalErrorException(BulkOperationsIsNotSupportedError);
            }

            var entity = input.Input[0];
            var objectDefName = input.Input[0].ObjectDefinitionFullName;
            var lookupCondition = input.LookupCondition[0];
            
            switch (input.Name)
            {
                case nameof(KnownActions.Create)
                when objectDefName == GetObjectDefinitionFullName<Organization>():
                    return await ExecuteOperation<Organization, Organization>(
                        entity,
                        lookupCondition,
                        CreateChildOrganizationAsync);
                case nameof(KnownActions.Create)
                when objectDefName == GetObjectDefinitionFullName<InvitedUser>():
                    return await ExecuteOperation<InvitedUser, InvitedUser>(
                        entity,
                        lookupCondition,
                        CreateInvitedUserAsync);
                case nameof(KnownActions.Create)
                when objectDefName == GetObjectDefinitionFullName<Connection>():
                    return await ExecuteOperation<Connection, Connection>(
                        entity,
                        lookupCondition,
                        CreateConnectionAsync);
                case nameof(KnownActions.Update)
                when objectDefName == typeof(Solution).GetObjectDefinitionFullName():
                    return await ExecuteOperation<Solution, Solution>(
                        entity,
                        lookupCondition,
                        UpdateSolutionAsync);
                case nameof(KnownActions.Delete)
                when objectDefName == typeof(Solution).GetObjectDefinitionFullName():
                    return await ExecuteOperation<Solution, Solution>(
                        entity,
                        lookupCondition,
                        DeleteSolutionAsync);
                case nameof(KnownActions.Delete)
                when objectDefName == typeof(OrganizationUser).GetObjectDefinitionFullName():
                    return await ExecuteOperation<OrganizationUser, OrganizationUser>(
                        entity,
                        lookupCondition,
                        DeleteOrganizationUserAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(CloneSolution).GetObjectDefinitionFullName():
                    return await ExecuteOperation<CloneSolution, CloneSolution>(
                        entity,
                        lookupCondition,
                        CloneSolutionAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(PrepareSolution).GetObjectDefinitionFullName():
                    return await ExecuteOperation<PrepareSolution, PrepareSolution>(
                        entity,
                        lookupCondition,
                        PrepareSolutionAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(StartSolution).GetObjectDefinitionFullName():
                    return await ExecuteOperation<StartSolution, object>(
                        entity,
                        lookupCondition,
                        StartSolutionAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(StopSolution).GetObjectDefinitionFullName():
                    return await ExecuteOperation<StopSolution, object>(
                        entity,
                        lookupCondition,
                        StopSolutionAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(MarkAllErrors).GetObjectDefinitionFullName():
                    return await ExecuteOperation<MarkAllErrors, object>(
                        entity,
                        lookupCondition,
                        MarkAllErrorsAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(ReprocessAllErrors).GetObjectDefinitionFullName():
                    return await ExecuteOperation<ReprocessAllErrors, object>(
                        entity,
                        lookupCondition,
                        ReprocessAllErrorsAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(RunMap).GetObjectDefinitionFullName():
                    return await ExecuteOperation<RunMap, RunMap>(
                        entity,
                        lookupCondition,
                        RunMapAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(TestConnection).GetObjectDefinitionFullName():
                    return await ExecuteOperation<TestConnection, TestConnection>(
                        entity,
                        lookupCondition,
                        TestConnectionAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(InstallConnector).GetObjectDefinitionFullName():
                    return await ExecuteOperation<InstallConnector, object>(
                        entity,
                        lookupCondition,
                        InstallConnectorAsync);
                case CommonActionNames.Command
                when objectDefName == typeof(ProvisionCloudAgent).GetObjectDefinitionFullName():
                    return await ExecuteOperation<ProvisionCloudAgent, object>(
                        entity,
                        lookupCondition,
                        ProvisionCloudAgentAsync);
                default:
                    throw new FatalErrorException(
                        string.Format(
                            OperationIsNotSupportedErrorFormat,
                            input.Name,
                            objectDefName));
            }
        }

        private async Task<OperationResult> ExecuteOperation<TInput, TOutput>(
            DataEntity entity,
            Expression lookupCondition,
            Func<TInput, Expression, Task<TOutput>> execute) where TInput : new()
        {
            var result = new OperationResult(size: 1) {Output = {[0] = new DataEntity()}};
            var input = entity.ToDomainObject<TInput>();
            try
            {
                var output = await execute(input, lookupCondition);
                result.ObjectsAffected[0] = output == null ? 0 : 1;
                result.Output[0] = output?.ToDataEntity();
                result.Success[0] = true;
            }
            catch (FatalErrorException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ScribeLogger.Instance.WriteException(this, ex);
                
                var rootCause = ex;
                while (rootCause is AggregateException)
                {
                    rootCause = rootCause.InnerException;
                }
                
                result.ErrorInfo[0] = new ErrorResult
                {
                    Number = 0,
                    Description = rootCause?.Message,
                    Detail = rootCause?.StackTrace
                };
            }
            return result;
        }

        private async Task<Organization> CreateChildOrganizationAsync(
            Organization input,
            Expression lookupCondition)
        {
            input.ParentId = input.ParentId ?? this.defaultOrganizationId;
            return await this.httpClient.CreateChildOrganizationAsync(input);
        }

        private async Task<InvitedUser> CreateInvitedUserAsync(
            InvitedUser input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<InvitedUser>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            var result = await this.httpClient.CreateInvitedUserAsync(input);
            result.OrganizationId = input.OrganizationId;
            return result;
        }
        
        private async Task<Connection> CreateConnectionAsync(
            Connection input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<Connection>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            var result = await this.httpClient.CreateConnectionAsync(input);
            result.OrganizationId = input.OrganizationId;
            return result;
        }
        
        [SuppressMessage(
            "ReSharper", 
            "PossibleInvalidOperationException", 
            Justification = 
                "OrganizationId: QueryExecutor will throw an exception on null value."
                + "Id: Scribe API always returns non null solution Id.")]
        private async Task<Solution> UpdateSolutionAsync(
            Solution input,
            Expression lookupCondition)
        {
            var query = QueryFor<Solution>(lookupCondition);
            // HACK: implementation details of Query Executor leaked
            var candidates = this.queryExecutor.ExtractCandidatesFrom(query);
            var toUpdate = this.queryExecutor.GetSolutions(candidates).ToList();

            if (!toUpdate.Any())
            {
                return null;
            }
            if (toUpdate.Count > 1)
            {
                throw new InvalidExecuteOperationException(
                    string.Format(
                        MoreThanOneEntityFoundErrorFormat,
                        KnownActions.Update,
                        GetObjectDefinitionFullName<Solution>()));
            }

            var requestEntity = toUpdate.Single();
            requestEntity = requestEntity.MergeWith(input);
            return await this.httpClient.UpdateSolutionAsync(
                requestEntity.OrganizationId.Value,
                requestEntity.Id.Value,
                requestEntity);
        }

        private static Query QueryFor<T>(Expression lookupCondition)
        {
            return new Query
            {
                RootEntity = new QueryEntity
                    {ObjectDefinitionFullName = GetObjectDefinitionFullName<T>()},
                Constraints = lookupCondition
            };
        }

        [SuppressMessage(
            "ReSharper", 
            "PossibleInvalidOperationException", 
            Justification = 
                "OrganizationId: QueryExecutor will throw an exception on null value."
                + "Id: Scribe API always returns non null solution Id.")]
        private async Task<Solution> DeleteSolutionAsync(
            Solution input,
            Expression lookupCondition)
        {
            var query = QueryFor<Solution>(lookupCondition);
            var toDelete = (await this.queryExecutor.Execute(query)).ToList();

            if (!toDelete.Any())
            {
                return null;
            }
            if (toDelete.Count > 1)
            {
                throw new InvalidExecuteOperationException(
                    string.Format(
                        MoreThanOneEntityFoundErrorFormat,
                        KnownActions.Delete,
                        GetObjectDefinitionFullName<Solution>()));
            }

            var requestEntity = toDelete.Single().ToDomainObject<Solution>();
            await this.httpClient.DeleteSolutionAsync(
                requestEntity.OrganizationId.Value,
                requestEntity.Id.Value);
            return requestEntity;
        }
        
        [SuppressMessage(
            "ReSharper", 
            "PossibleInvalidOperationException", 
            Justification = 
                "OrganizationId: QueryExecutor will throw an exception on null value.")]
        private async Task<OrganizationUser> DeleteOrganizationUserAsync(
            OrganizationUser input,
            Expression lookupCondition)
        {
            var query = QueryFor<OrganizationUser>(lookupCondition);
            var toDelete = (await this.queryExecutor.Execute(query)).ToList();

            if (!toDelete.Any())
            {
                return null;
            }
            if (toDelete.Count > 1)
            {
                throw new InvalidExecuteOperationException(
                    string.Format(
                        MoreThanOneEntityFoundErrorFormat,
                        KnownActions.Delete,
                        GetObjectDefinitionFullName<OrganizationUser>()));
            }

            var requestEntity = toDelete.Single().ToDomainObject<OrganizationUser>();
            await this.httpClient.DeleteOrganizationUserAsync(
                requestEntity.OrganizationId.Value,
                requestEntity.Email);
            return requestEntity;
        }

        private async Task<CloneSolution> CloneSolutionAsync(
            CloneSolution input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<CloneSolution>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            var result = await this.httpClient.CloneSolutionAsync(input);
            result.OrganizationId = input.OrganizationId;
            result.SolutionId = input.SolutionId;
            result.DestinationOrganizationId = input.DestinationOrganizationId;
            result.DestinationAgentId = input.DestinationAgentId;
            return result;
        }
        
        private async Task<PrepareSolution> PrepareSolutionAsync(
            PrepareSolution input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<PrepareSolution>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            
            var result = await this.httpClient.PrepareSolutionAsync(input);
            result.OrganizationId = input.OrganizationId;
            result.SolutionId = input.SolutionId;
            
            for (var i = 0; i < this.retryCount; i++)
            {
                result = (await this.httpClient.GetSolutionPreparationResultAsync(result));
                result.OrganizationId = input.OrganizationId;
                result.SolutionId = input.SolutionId;
                
                if (result.IsComplete != true)
                {
                    await Task.Delay(this.delay);
                    continue;
                }
                break;
            }
            // This delay is just to workaround API issue: it can return 400 response on 
            // start solution request even after preparation was completed (IsComplete == true).
            // Error message from API: "RunProcessNow: EventInfo <id> not found cannot run now".
            await Task.Delay(this.delay);

            return result;
        }
        
        private async Task<TestConnection> TestConnectionAsync(
            TestConnection input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<TestConnection>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            
            var result = await this.httpClient.TestConnectionAsync(input);
            result.OrganizationId = input.OrganizationId;
            result.ConnectionId = input.ConnectionId;
            result.AgentId = input.AgentId;
            
            for (var i = 0; i < this.retryCount; i++)
            {
                result = await this.httpClient.GetTestConnectionResultAsync(result);
                result.OrganizationId = input.OrganizationId;
                result.ConnectionId = input.ConnectionId;
                result.AgentId = input.AgentId;
                
                if (!string.Equals(result.Status, "Completed", StringComparison.InvariantCultureIgnoreCase))
                {
                    await Task.Delay(this.delay);
                    continue;
                }
                break;
            }

            return result;
        }

        private async Task<object> StartSolutionAsync(
            StartSolution input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<StartSolution>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            return await this.httpClient.StartSolutionAsync(input);
        }

        private async Task<object> StopSolutionAsync(
            StopSolution input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<StopSolution>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            return await this.httpClient.StopSolutionAsync(input);
        }

        private async Task<object> MarkAllErrorsAsync(
            MarkAllErrors input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<MarkAllErrors>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            return await this.httpClient.MarkErrorsAsync(input);
        }

        private async Task<object> ReprocessAllErrorsAsync(
            ReprocessAllErrors input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<ReprocessAllErrors>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            return await this.httpClient.ReprocessErrorsAsync(input);
        }

        private async Task<RunMap> RunMapAsync(
            RunMap input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<RunMap>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            var result = await this.httpClient.RunMapAsync(input);
            result.OrganizationId = input.OrganizationId;
            result.SolutionId = input.SolutionId;
            result.MapId = input.MapId;
            return result;
        }

        private async Task<object> InstallConnectorAsync(
            InstallConnector input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<InstallConnector>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            return await this.httpClient.InstallConnectorAsync(input);
        }

        [SuppressMessage(
            "ReSharper", 
            "PossibleInvalidOperationException", 
            Justification = "OrganizationId: GetRequiredOrganizationId will throw an exception on null value.")]
        private async Task<object> ProvisionCloudAgentAsync(
            ProvisionCloudAgent input,
            Expression lookupCondition)
        {
            input.OrganizationId = GetRequiredOrganizationId<ProvisionCloudAgent>(
                input.OrganizationId,
                nameof(input.OrganizationId));
            return await this.httpClient.ProvisionCloudAgentAsync(input.OrganizationId.Value);
        }

        private int? GetRequiredOrganizationId<T>(int? inputValue, string propertyName)
        {
            return inputValue
                ?? this.defaultOrganizationId
                ?? throw new FatalErrorException(
                       string.Format(
                           RequiredPropertyIsMissingInInputErrorFormat,
                           GetPropertyDefinitionFullName<T>(propertyName),
                           GetObjectDefinitionFullName<T>()));
        }
    }
}
