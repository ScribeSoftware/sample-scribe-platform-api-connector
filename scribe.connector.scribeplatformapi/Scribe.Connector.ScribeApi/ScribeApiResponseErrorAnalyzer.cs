//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using Newtonsoft.Json;
    using static Newtonsoft.Json.JsonConvert;

    public static class ScribeApiResponseErrorAnalyzer
    {
        public static void ThrowOnFailureStatusCode(this HttpResponseMessage response)
        {
            if (response == null)
            {
                throw new ArgumentNullException(nameof(response));
            }
            if (response.IsSuccessStatusCode)
            {
                return;
            }
            if (response.Content == null)
            {
                throw new HttpRequestException(response.CreateErrorMessage());
            }

            try
            {
                var content = response.Content.ReadAsStringAsync().Result.Trim();
                switch (response.Content.Headers.ContentType?.MediaType)
                {
                    case "text/plain":
                        throw new HttpRequestException(
                            response.CreateErrorMessage(content));
                    case "application/json":
                        try
                        {
                            var error = DeserializeObject<ScribeApiError>(content);
                            throw new HttpRequestException(
                                response.CreateErrorMessage(error.ToString()));
                        }
                        catch (JsonReaderException)
                        {
                            // When accessing non existing resource, Scribe API returns
                            // 404 response with plain text error but "application/json"
                            // content type.
                            throw new HttpRequestException(
                                response.CreateErrorMessage(content));
                        }
                    default:
                        throw new HttpRequestException(
                            response.CreateErrorMessage());
                }
            }
            finally
            {
                response.Content.Dispose();
            }
        }

        private static string CreateErrorMessage(
            this HttpResponseMessage response,
            string errorDetails = "")
        {
            return $"{(int) response.StatusCode} ({response.ReasonPhrase}) {errorDetails}".Trim();
        }
        
        public class ScribeApiError
        {
            public string Message { get; set; }
            public IDictionary<string, IEnumerable<string>> ModelState { get; set; }

            public override string ToString()
            {
                var errorBuilder = new StringBuilder();
                errorBuilder.AppendLine($"{Message}");
                if (ModelState != null)
                {
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value)
                        {
                            errorBuilder.AppendLine($" {state.Key}: {error.Trim('.')}.");
                        }
                    }
                }
                return errorBuilder.ToString();
            }
        }
    }
}
