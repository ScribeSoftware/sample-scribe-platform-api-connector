//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
namespace Scribe.Connector.ScribeApi.Http
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading;
    using System.Threading.Tasks;

    public class SingleMediaTypeHttpClient
    {
        private readonly Action<HttpResponseMessage> responseAnalyzer;

        public SingleMediaTypeHttpClient(
            HttpClient httpClient,
            MediaTypeFormatter formatter) 
            : this (httpClient, formatter, r => r.EnsureSuccessStatusCode())
        {
        }
        
        public SingleMediaTypeHttpClient(
            HttpClient httpClient,
            MediaTypeFormatter formatter,
            Action<HttpResponseMessage> responseAnalyzer)
        {
            if (httpClient == null)
            {
                throw new ArgumentNullException(nameof(httpClient));
            }
            if (formatter == null)
            {
                throw new ArgumentNullException(nameof(formatter));
            }
            if (responseAnalyzer == null)
            {
                throw new ArgumentNullException(nameof(responseAnalyzer));
            }

            HttpClient = httpClient;
            Formatter = formatter;
            this.responseAnalyzer = responseAnalyzer;
        }
        
        public HttpClient HttpClient { get; }
        public MediaTypeFormatter Formatter { get; }

        public async Task<TResp> SendMediaContentAsync<TReq, TResp>(
            HttpMethod httpMethod,
            Uri uri,
            TReq requestBody = default(TReq),
            HttpCompletionOption completionOption = HttpCompletionOption.ResponseHeadersRead,
            CancellationToken token = default(CancellationToken))
        {
            var content = httpMethod == HttpMethod.Get
                ? null
                : new ObjectContent<TReq>(requestBody, Formatter);
            var request = new HttpRequestMessage(httpMethod, uri) {Content = content};
            var response = await HttpClient.SendAsync(request, completionOption, token);
            this.responseAnalyzer(response);
            return response.StatusCode == HttpStatusCode.NoContent || response.Content == null
                ? default(TResp)
                : await response.Content.ReadAsAsync<TResp>(
                    new[] {Formatter},
                    token);
        }
    }
}
