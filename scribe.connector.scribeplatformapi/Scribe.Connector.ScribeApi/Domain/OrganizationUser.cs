//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
namespace Scribe.Connector.ScribeApi.Domain
{
    using Core.ConnectorApi.Metadata;
    using Metadata;

    [ObjectDefinition]
    [SupportedAction(KnownActions.Query)]
    [SupportedAction(KnownActions.Delete)]
    public class OrganizationUser
    {
        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true)]
        public string Email { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string FirstName { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string LastName { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string Role { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public bool? PendingInvitation { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true)]
        public int? OrganizationId { get; set; }
    }
}
