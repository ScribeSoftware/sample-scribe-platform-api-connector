//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
namespace Scribe.Connector.ScribeApi.Domain
{
    using Core.ConnectorApi.Metadata;
    using Metadata;

    [ObjectDefinition]
    [SupportedAction(KnownActions.Query)]
    [SupportedAction(KnownActions.Create)]
    public class Organization
    {
        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = false,
            UsedInLookupCondition = false,
            IsPrimaryKey = true)]
        public int? Id { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = true)]
        public string Name { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = true)]
        public int? ParentId { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public int? TenantType { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string Website { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string Street { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string City { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string State { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PostalCode { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string Country { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactFirstName { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactLastName { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactEmail { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactPhoneNumber { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactStreet { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactCity { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactState { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactPostalCode { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string PrimaryContactCountry { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public bool? IsSourceDataLocal { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string ApiToken { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public bool? IsAgentLogDownloadAllowed { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string Status { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string TenantRole { get; set; }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(ParentId)}: {ParentId}, {nameof(ApiToken)}: {ApiToken}";
        }
    }
}
