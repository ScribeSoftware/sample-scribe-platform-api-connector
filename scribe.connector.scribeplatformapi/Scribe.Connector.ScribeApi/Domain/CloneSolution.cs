//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Domain
{
    using System;
    using Core.ConnectorApi.Metadata;
    using Metadata;

    [ObjectDefinition]
    [SupportedAction(KnownActions.Create, FullName = CommonActionNames.Command)]
    public class CloneSolution
    {
        // Input
        [PropertyDefinition(
            RequiredInActionInput = false,
            UsedInActionInput = true,
            UsedInActionOutput = false)]
        public int? OrganizationId { get; set; }

        [PropertyDefinition(
            RequiredInActionInput = true,
            UsedInActionInput = true,
            UsedInActionOutput = false)]
        public Guid? SolutionId { get; set; }

        [PropertyDefinition(
            RequiredInActionInput = true,
            UsedInActionInput = true,
            UsedInActionOutput = false)]
        public Guid? DestinationAgentId { get; set; }

        [PropertyDefinition(
            RequiredInActionInput = true,
            UsedInActionInput = true,
            UsedInActionOutput = false)]
        public int? DestinationOrganizationId { get; set; }

        // Output
        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string Id { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string Name { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public Guid? AgentId { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string Description { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string ConnectionIdForSource { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string ConnectionIdForTarget { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string SolutionType { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string Status { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string InProgressStartTime { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string LastRunTime { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string NextRunTime { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public bool? IsDisabled { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public int? ReasonDisabled { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string MinAgentVersion { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string ModificationBy { get; set; }
    }
}
