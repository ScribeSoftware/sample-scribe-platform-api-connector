//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Domain
{
    using System;
    using System.Collections.Generic;
    using Core.ConnectorApi.Metadata;
    using Metadata;

    [ObjectDefinition]
    [SupportedAction(KnownActions.Query)]
    [SupportedAction(KnownActions.Delete)]
    [SupportedAction(KnownActions.Update)]
    public class Solution
    {
        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true,
            IsPrimaryKey = true)]
        public Guid? Id { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true)]
        public string Name { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public Guid? AgentId { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public string Description { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public Guid? ConnectionIdForSource { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public Guid? ConnectionIdForTarget { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true)]
        public string SolutionType { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true)]
        public string Status { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string InProgressStartTime { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string LastRunTime { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string NextRunTime { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public bool? IsDisabled { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public int? ReasonDisabled { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string MinAgentVersion { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false)]
        public string ModificationBy { get; set; }

        public IEnumerable<MapLink> MapLinks { get; set; }
        public ReplicationSettingsEntitySelection ReplicationSettings { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = true,
            UsedInActionInput = false,
            RequiredInActionInput = false,
            UsedInQueryConstraint = true,
            UsedInLookupCondition = true)]
        public int? OrganizationId { get; set; }
        
        public Solution MergeWith(Solution other)
        {
            return new Solution
            {
                AgentId = other.AgentId ?? AgentId,
                ConnectionIdForSource = other.ConnectionIdForSource ?? ConnectionIdForSource,
                ConnectionIdForTarget = other.ConnectionIdForTarget ?? ConnectionIdForTarget,
                Description = other.Description ?? Description,
                Id = other.Id ?? Id,
                InProgressStartTime = other.InProgressStartTime ?? InProgressStartTime,
                IsDisabled = other.IsDisabled ?? IsDisabled,
                LastRunTime = other.LastRunTime ?? LastRunTime,
                MinAgentVersion = other.MinAgentVersion ?? MinAgentVersion,
                ModificationBy = other.ModificationBy ?? ModificationBy,
                Name = other.Name ?? Name,
                NextRunTime = other.NextRunTime ?? NextRunTime,
                OrganizationId = other.OrganizationId ?? OrganizationId,
                ReasonDisabled = other.ReasonDisabled ?? ReasonDisabled,
                SolutionType = other.SolutionType ?? SolutionType,
                Status = other.Status ?? Status,
                MapLinks = other.MapLinks ?? MapLinks,
                ReplicationSettings = other.ReplicationSettings ?? ReplicationSettings
            };
        }
    }
}
