//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi.Domain
{
    using System;
    using Core.ConnectorApi.Metadata;
    using Metadata;

    [ObjectDefinition]
    [SupportedAction(KnownActions.Create, FullName = CommonActionNames.Command)]
    public class MarkAllErrors 
    {
        [PropertyDefinition(
            UsedInActionOutput = false,
            UsedInActionInput = true,
            RequiredInActionInput = true)]
        public bool? Mark { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = false,
            UsedInActionInput = true,
            RequiredInActionInput = false)]
        public int? OrganizationId { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = false,
            UsedInActionInput = true,
            RequiredInActionInput = true)]
        public Guid? SolutionId { get; set; }

        [PropertyDefinition(
            UsedInActionOutput = false,
            UsedInActionInput = true,
            RequiredInActionInput = true)]
        public string HistoryId { get; set; }
    }
}
