//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
// This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
// you a nonexclusive copyright license to use all programming code examples from which you can generate similar
// functionality tailored to your own specific needs.
//
// These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
// any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
// functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
// a particular purpose are expressly disclaimed.
//
// For more details and documentation please visit:  
// https://dev.scribesoft.com/en/ref_app/api_conn/scribe_api_connector.htm  
// 
// https://dev.scribesoft.com
//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

namespace Scribe.Connector.ScribeApi
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Core.ConnectorApi;
    using Core.ConnectorApi.Exceptions;
    using Core.ConnectorApi.Query;
    using Domain;
    using Http;
    using Metadata;
    using static Metadata.CandidateExtensions;
    using static Metadata.ReflectionExtensions;

    public class ScribeApiQueryExecutor
    {
        private const string EntityIsNotSupportedErrorFormat =
            "{0} entity is not supported.";
        private const string QueryNotSupportedErrorFormat =
            "Filters and lookup conditions are not supported for {0} entity.";
        private const string QueryIsMissingErrorFormat =
            "Filter or lookup condition are required for quering {0} entity.";
        private const string QueryIsInvalidError =
            "Filter or lookup condition are invalid";
        
        private readonly SingleMediaTypeHttpClient httpClient;
        private readonly ExpressionVisitor expressionVisitor;
        private readonly CandidateValidator validator;
        private readonly int? defaultOrganizationId;

        public ScribeApiQueryExecutor(
            SingleMediaTypeHttpClient httpClient,
            ExpressionVisitor expressionVisitor,
            CandidateValidator validator,
            int? defaultOrganizationId)
        {
            if (httpClient == null)
            {
                throw new ArgumentNullException(nameof(httpClient));
            }
            if (expressionVisitor == null)
            {
                throw new ArgumentNullException(nameof(expressionVisitor));
            }
            if (validator == null)
            {
                throw new ArgumentNullException(nameof(validator));
            }

            this.httpClient = httpClient;
            this.expressionVisitor = expressionVisitor;
            this.validator = validator;
            this.defaultOrganizationId = defaultOrganizationId;
        }

        public async Task<IEnumerable<DataEntity>> Execute(Query query)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            var candidates = ExtractCandidatesFrom(query);

            switch (query.RootEntity.ObjectDefinitionFullName)
            {
                case nameof(Organization):
                    return await GetOrganizationsAsync(candidates);
                case nameof(Solution):
                    return await Task.FromResult(
                        GetSolutions(candidates).Select(x => x.ToDataEntity()));
                case nameof(InvitedUser):
                    return await GetInvitedUsers(candidates);
                case nameof(History):
                    return await Task.FromResult(GetHistories(candidates));
                case nameof(Error):
                    return await Task.FromResult(GetErrors(candidates));
                case nameof(FailedRecord):
                    return await GetFailedRecords(candidates);
                case nameof(OrganizationUser):
                    return await Task.FromResult(GetOrganizationUsers(candidates));
                case nameof(Map):
                    return await Task.FromResult(GetMaps(candidates));
                case nameof(Connection):
                    return await Task.FromResult(GetConnections(candidates));
                case nameof(Connector):
                    return await Task.FromResult(GetConnectors(candidates));
                case nameof(Agent):
                    return await Task.FromResult(GetAgents(candidates));
                default:
                    throw new FatalErrorException(
                        string.Format(
                            EntityIsNotSupportedErrorFormat,
                            query.RootEntity.ObjectDefinitionFullName));
            }
        }

        internal List<IDictionary<string, string>> ExtractCandidatesFrom(Query query)
        {
            var candidates = new List<IDictionary<string, string>>(0);
            if (query.Constraints != null)
            {
                try
                {
                    candidates = this.expressionVisitor.Visit(query.Constraints).ToList();
                }
                catch (InvalidOperationException ex)
                {
                    throw new FatalErrorException(ex.Message, ex);
                }
                var errors = candidates.SelectMany(this.validator.Validate).ToList();
                if (errors.Any())
                {
                    // HTML tags are just for making error messages more readable in UI
                    var messageBuilder = new StringBuilder($"{QueryIsInvalidError}: <br />");
                    errors.ForEach(e => messageBuilder.AppendLine($"• {e.Value} <br />"));
                    throw new FatalErrorException(messageBuilder.ToString());
                }
            }
            return candidates;
        }

        [SuppressMessage(
            "ReSharper",
            "UnusedParameter.Local",
            Justification = "Parameter 'candidates' used only for preconditions check.")]
        private async Task<IEnumerable<DataEntity>> GetOrganizationsAsync(
            List<IDictionary<string, string>> candidates)
        {
            if (candidates.Any())
            {
                throw new FatalErrorException(QueryNotSupportedError<Organization>());
            }

            return (await this.httpClient
                .GetOrganizationsAsync())
                .Select(x => x.ToDataEntity());
        }

        internal IEnumerable<Solution> GetSolutions(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                candidates = CreateDefaultCandidates<Solution>(
                    nameof(Solution.OrganizationId));
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<Solution, int?>(
                    nameof(Solution.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var id = candidate.GetValueOrDefault<Solution, Guid?>(
                    nameof(Solution.Id));
                var name = candidate.GetValueOrDefault<Solution, string>(
                    nameof(Solution.Name));
                var type = candidate.GetValueOrDefault<Solution, string>(
                    nameof(Solution.SolutionType));
                var status = candidate.GetValueOrDefault<Solution, string>(
                    nameof(Solution.Status));

                if (id == null)
                {
                    var solutions = this.httpClient
                        .GetSolutionsAsync(orgId, name, type, status)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            return x;
                        });

                    foreach (var solution in solutions)
                    {
                        yield return solution;
                    }
                }
                else
                {
                    var solution = this.httpClient
                        .GetSolutionAsync(orgId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    solution.OrganizationId = orgId;

                    if (name != null && solution.Name != name
                        || type != null && solution.SolutionType != type
                        || status != null && solution.Status != status)
                    {
                        continue;
                    }
                    yield return solution;
                }
            }
        }

        private async Task<IEnumerable<DataEntity>> GetInvitedUsers(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                candidates = CreateDefaultCandidates<InvitedUser>(
                    nameof(InvitedUser.OrganizationId));
            }

            var result = new List<DataEntity>();
            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<InvitedUser, int?>(
                    nameof(InvitedUser.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var email = candidate.GetValueOrDefault<InvitedUser, string>(
                    nameof(InvitedUser.Email));

                if (email == null)
                {
                    var users = (await this.httpClient
                        .GetInvitedUsersAsync(orgId))
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            return x.ToDataEntity();
                        });
                    result.AddRange(users);
                }
                else
                {
                    var user = await this.httpClient.GetInvitedUserAsync(orgId, email);
                    user.OrganizationId = orgId;
                    result.Add(user.ToDataEntity());
                }
            }

            return result;
        }

        private IEnumerable<DataEntity> GetHistories(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                throw new FatalErrorException(MissingQueryError<History>());
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<History, int?>(
                    nameof(History.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var solutionId = candidate.GetRequiredValueOrThrow<History, Guid>(
                    nameof(History.SolutionId));
                var id = candidate.GetValueOrDefault<History, long?>(
                    nameof(History.Id));
                
                CheckOptionalLongPropertyIsNotNull<History>(
                    candidate, nameof(History.Id), id);

                if (id == null)
                {
                    var histories = this.httpClient
                        .GetHistoriesAsync(orgId, solutionId)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            x.SolutionId = solutionId;
                            return x.ToDataEntity();
                        });
                    
                    foreach (var entity in histories)
                    {
                        yield return entity;
                    }
                }
                else
                {
                    var history = this.httpClient
                        .GetHistoryAsync(orgId, solutionId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    history.OrganizationId = orgId;
                    history.SolutionId = solutionId;
                    yield return history.ToDataEntity();
                }
            }
        }

        private IEnumerable<DataEntity> GetErrors(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                throw new FatalErrorException(MissingQueryError<Error>());
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<Error, int?>(
                    nameof(Error.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var solutionId = candidate.GetRequiredValueOrThrow<Error, Guid>(
                    nameof(Error.SolutionId));
                var historyId = candidate.GetRequiredValueOrThrow<Error, long>(
                    nameof(Error.HistoryId));
                var id = candidate.GetValueOrDefault<Error, long?>(
                    nameof(Error.Id));
                
                CheckOptionalLongPropertyIsNotNull<Error>(
                    candidate, nameof(Error.Id), id);

                if (id == null)
                {
                    var errors = this.httpClient
                        .GetErrorsAsync(orgId, solutionId, historyId)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            x.SolutionId = solutionId;
                            x.HistoryId = historyId.ToString(CultureInfo.InvariantCulture);
                            return x.ToDataEntity();
                        });
                    
                    foreach (var entity in errors)
                    {
                        yield return entity;
                    }
                }
                else
                {
                    var error = this.httpClient
                        .GetErrorAsync(orgId, solutionId, historyId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    error.OrganizationId = orgId;
                    error.SolutionId = solutionId;
                    error.HistoryId = historyId.ToString(CultureInfo.InvariantCulture);
                    yield return error.ToDataEntity();
                }
            }
        }

        private async Task<IEnumerable<DataEntity>> GetFailedRecords(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                throw new FatalErrorException(MissingQueryError<FailedRecord>());
            }

            var result = new List<DataEntity>();
            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<FailedRecord, int?>(
                    nameof(FailedRecord.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var solutionId = candidate.GetRequiredValueOrThrow<FailedRecord, Guid>(
                    nameof(FailedRecord.SolutionId));
                var historyId = candidate.GetRequiredValueOrThrow<FailedRecord, long>(
                    nameof(FailedRecord.HistoryId));
                var id = candidate.GetRequiredValueOrThrow<FailedRecord, long>(
                    nameof(FailedRecord.Id));

                var record = await this.httpClient.GetFailedRecordAsync(orgId, solutionId, historyId, id);
                record.OrganizationId = orgId;
                record.SolutionId = solutionId;
                record.HistoryId = historyId.ToString(CultureInfo.InvariantCulture);
                record.Id = id.ToString(CultureInfo.InvariantCulture);
                result.Add(record.ToDataEntity());
            }

            return result;
        }

        private IEnumerable<DataEntity> GetOrganizationUsers(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                candidates = CreateDefaultCandidates<OrganizationUser>(
                    nameof(OrganizationUser.OrganizationId));
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<OrganizationUser, int?>(
                    nameof(OrganizationUser.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var email = candidate.GetValueOrDefault<OrganizationUser, string>(
                    nameof(OrganizationUser.Email));

                var users = this.httpClient
                    .GetOrganizationUsersAsync(orgId)
                    .GetAwaiter()
                    .GetResult();;
                if (email == null)
                {
                    foreach (var user in users)
                    {
                        user.OrganizationId = orgId;
                        yield return user.ToDataEntity();
                    }
                }
                else
                {
                    foreach (var user in users.Where(x => x.Email == email))
                    {
                        user.OrganizationId = orgId;
                        yield return user.ToDataEntity();
                    }
                }
            }
        }

        private IEnumerable<DataEntity> GetMaps(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                throw new FatalErrorException(MissingQueryError<Map>());
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<Map, int?>(
                    nameof(Map.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var solutionId = candidate.GetRequiredValueOrThrow<Map, Guid>(
                    nameof(Map.SolutionId));
                var id = candidate.GetValueOrDefault<Map, int?>(
                    nameof(Map.Id));

                if (id == null)
                {
                    var maps = this.httpClient
                        .GetMapsAsync(orgId, solutionId)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            x.SolutionId = solutionId;
                            var serializedMap = this.httpClient.Formatter
                                .WriteToStringAsync(x)
                                .GetAwaiter()
                                .GetResult();;
                            x.MapJson = serializedMap;
                            return x.ToDataEntity();
                        });
                    
                    foreach (var entity in maps)
                    {
                        yield return entity;
                    }
                }
                else
                {
                    var map = this.httpClient
                        .GetMapAsync(orgId, solutionId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    map.OrganizationId = orgId;
                    map.SolutionId = solutionId;
                    var serializedMap = this.httpClient.Formatter.WriteToStringAsync(map)
                        .GetAwaiter()
                        .GetResult();
                    map.MapJson = serializedMap;

                    yield return map.ToDataEntity();
                }
            }
        }

        private IEnumerable<DataEntity> GetConnections(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                candidates = CreateDefaultCandidates<Connection>(
                    nameof(Connection.OrganizationId));
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<Connection, int?>(
                    nameof(Connection.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var name = candidate.GetValueOrDefault<Connection, string>(
                    nameof(Connection.Name));
                var id = candidate.GetValueOrDefault<Connection, Guid?>(
                    nameof(Connection.Id));

                if (id == null)
                {
                    var connections = this.httpClient
                        .GetConnectionsAsync(orgId, name)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            return x.ToDataEntity();
                        });

                    foreach (var dataEntity in connections)
                    {
                        yield return dataEntity;
                    }
                }
                else
                {
                    var connection = this.httpClient
                        .GetConnectionAsync(orgId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    connection.OrganizationId = orgId;

                    if (name != null && connection.Name != name)
                    {
                        continue;
                    }
                    yield return connection.ToDataEntity();
                }
            }
        }

        private IEnumerable<DataEntity> GetConnectors(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                candidates = CreateDefaultCandidates<Connector>(
                    nameof(Connector.OrganizationId));
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<Connector, int?>(
                    nameof(Connector.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var id = candidate.GetValueOrDefault<Connector, Guid?>(
                    nameof(Connector.Id));

                if (id == null)
                {
                    var connectors = this.httpClient
                        .GetConnectorsAsync(orgId)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            return x.ToDataEntity();
                        });
                    
                    foreach (var dataEntity in connectors)
                    {
                        yield return dataEntity;
                    }
                }
                else
                {
                    var connector = this.httpClient
                        .GetConnectorAsync(orgId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    connector.OrganizationId = orgId;
                    yield return connector.ToDataEntity();
                }
            }
        }

        private IEnumerable<DataEntity> GetAgents(
            List<IDictionary<string, string>> candidates)
        {
            if (!candidates.Any())
            {
                candidates = CreateDefaultCandidates<Agent>(
                    nameof(Agent.OrganizationId));
            }

            foreach (var candidate in candidates)
            {
                var orgId = candidate.GetRequiredValueOrThrow<Agent, int?>(
                    nameof(Agent.OrganizationId),
                    this.defaultOrganizationId) ?? 0;
                var id = candidate.GetValueOrDefault<Agent, Guid?>(
                    nameof(Agent.Id));

                if (id == null)
                {
                    var agents = this.httpClient
                        .GetAgentsAsync(orgId)
                        .GetAwaiter()
                        .GetResult()
                        .Select(x =>
                        {
                            x.OrganizationId = orgId;
                            return x.ToDataEntity();
                        });
                    
                    foreach (var dataEntity in agents)
                    {
                        yield return dataEntity;
                    }
                }
                else
                {
                    var connector = this.httpClient
                        .GetAgentAsync(orgId, id.Value)
                        .GetAwaiter()
                        .GetResult();
                    connector.OrganizationId = orgId;
                    yield return connector.ToDataEntity();
                }
            }
        }

        private List<IDictionary<string, string>> CreateDefaultCandidates<T>(
            string propertyNameForOrganizationId)
        {
            var key = typeof(T).GetPropertyDefinitionFullName(propertyNameForOrganizationId);
            return new List<IDictionary<string, string>>(1)
            {
                new Dictionary<string, string>(1)
                {
                    [key] = Convert.ToString(
                        this.defaultOrganizationId,
                        CultureInfo.InvariantCulture)
                }
            };
        }

        private static string QueryNotSupportedError<T>()
        {
            return string.Format(
                QueryNotSupportedErrorFormat,
                GetObjectDefinitionFullName<T>());
        }

        private static string MissingQueryError<T>()
        {
            return string.Format(
                QueryIsMissingErrorFormat,
                GetObjectDefinitionFullName<T>());
        }
        
        private static void CheckOptionalLongPropertyIsNotNull<T>(
            IDictionary<string, string> candidate,
            string propertyName,
            long? value)
        {
            // Enforce exception in case when candidate contains Id with null string.
            // Without this check all entities will be returned, which is not expected.
            // Root cause: HistoryId, FailedRecordId and ErrorId should be of long type 
            // instead of string, but Scribe incorrectly fills expressions with longs
            // (they are of double type).
            // We can remove this check after Scribe will fix the issue.
            var fullName = GetQueryEntityPropertyName<T>(propertyName);
            if (candidate.ContainsKey(fullName) && value == null)
            {
                throw new FatalErrorException(
                    $"Can't convert '{candidate[fullName]}' to type"
                    + $" {typeof(long).Name} for property {fullName}."); 
            }
        }
    }
}
